# PiClampZero
This repository describes the design for a leaf/stem clamp with built in camera to capture a time series of images as a plant dries down in order to assess cavitation of vessels.

#Camera Design
The PiClampZero are based on PiClamps that used Raspberry PI 3s and raspberry pi cameras. This variant uses ["Raspberry Pi zero W"](https://raspberry.piaustralia.com.au/raspberry-pi-zero-w) single board computers with an 8M pixel Pi Cam V2 camera with the option to use hand lens to magnify. PI zeros have in built WiFi and this is used to network the phenocams.
The PiZeroClamp consists of a set of 3D printed parts that constitute the holder for the PIZero and camera, a top housing to hold some electronics, a lens holder, a top and bottom lighting mount and the adjustable clamp to hold the leaf/stem.
The pi zero W and camera are housed in a standard housing which is mounted on a baseplate (3d printed)  


The Pi Zeros are mounted in the standard PI Zero housing which is then fitted into a custom build 3D printed [mounting plate]![Alt text](/pictures/IMG_20190306_115909_2.jpg)

# Parts list for PiClampZero

+ Raspberry Pi zero W, best to get starter kit that has case and extra cables and camera connector. [e.g. ](https://www.littlebird.com.au/raspberry-pi-zero-w-essentials-kit)
+ Raspberry cam V2 [e.g. ](https://raspberry.piaustralia.com.au/raspberry-pi-camera-board-v2)
+ power supply 12V >=1A [e.g. ](https://www.jaycar.com.au/switchmode-mains-adaptor-12vdc-1-5a/p/MP3486)
+ micro SD cards. I use class 10 16Gb [e.g.](https://www.officeworks.com.au/shop/officeworks/p/sandisk-ultra-16gb-micro-sdhc-memory-card-sdsq16gb) 

+ 3D printed parts see below
+ 2N7000 mosfet [e.g. from](https://au.element14.com/on-semiconductor/2n7000/mosfet-n-channel-200ma-60v-to/dp/9845178?scope=partnumberlookahead&ost=2N7000&searchref=searchlookahead&exaMfpn=true&ddkey=https%3Aen-AU%2FElement14_Australia%2Fw%2Fsearch)
+ 10K ohm resistor (pull down for Mosfet)
+ Barrel connector (female) to suit 12V power supply above [e.g.](https://au.element14.com/cliff-electronic-components/fc681446/socket-chassis-dc-power-2-5mm/dp/2285054)
+ LED light socket [e.g ](https://au.element14.com/lumberg/1502-03/socket-3-5mm-jack-chassis/dp/1270966?st=3.5%20mm%20jack)
+ LED light power plug 3.5mm mono right angle plug [e.g. ](https://au.element14.com/lumberg/wkls-2/plug-3-5mm-jack-r-a-mono/dp/1243265?st=3.5%20mm%20jack)
+ Molex crimp [connector](https://au.element14.com/molex/16-02-0083/contact-socket-24awg-crimp/dp/1756732)
+ Molex [recepticle](https://au.element14.com/molex/22-55-2101/connector-rcpt-10pos-2row-2-54mm/dp/1463043)
+ Red and Yellow indicator LEDS (jaycar or element 14)
+ resistors for indicator LEDS 330 Ohm
+ UBEC 12V to 5V regulator [e.g ](https://www.littlebird.com.au/ubec-dc-dc-step-down-buck-converter-5v-3a-output)
+ LED lighting strip 12V. I used non-waterproof version. [e.g. ](https://au.element14.com/plazmo-industries/29902900/led-strip-light-12v-20w-white/dp/2839721?st=led%20strip)
+ Coloured wire for linking up 

+ soldering iron/station [e.g. ](https://au.element14.com/tenma/21-10130-uk-eu/rework-station-900w-220v-uk-eu/dp/2062633?MER=bn_browse_1TP_MostPopular_2) with built in hot air for heat shrinking etc 
		[or ](https://www.jaycar.com.au/48w-temperature-controlled-soldering-station/p/TS1620)  if you want to save money.

#3D printed parts
The PiClampZero consists of a number of 3D printed parts designed in OnShape [(found here)](https://cad.onshape.com/documents/a506260739fc724f4f2b17e3/w/90d5fedff5a0a22d22a85460/e/7401e49a68706890f79e0fc7). 

+ bottom light and light diffuser. [.stl file](/3Dprint/PiZeroHolder - BottomLEDPlate.stl)

The camera assembly attaches to the bottom light with a novel adjustable hinge which allows for varying stem thicknesses to be easily clamped

*  camera assembly
       *   baseplate has 4 100mm M4 bolts that the remaining parts slide on.[.stl file](/3Dprint/circularspacer - Spacer (1).stl)
       * four focus adjustment wheels. [.stl file](/3Dprint/focusWheel - FocusWheel.stl)
       * top light ring - attached to baseplate. [.stl file](/3Dprint/LED_Ring - LEDRing.stl)
       * lens holder - attached to PiCam mount used for mounting hand lens (the current holder is for an Eshenbach x20 hand lens)[.stl file](/3Dprint/lensholder - LensHolder.stl)
       * PiCam mount designed to hold the standard PiZero plastic housing.[.stl file](/3Dprint/PiZeroHolder - PiZeroMount.stl)
       * TopCover - holds electronics and plug sockets to 12V power and top/bottom light source connector.[.stl file](/3Dprint/PiZeroHolder - TopCover.stl)

The leaf or stem is clamped between the bottom light and the camera assembly base plate. Then the focus wheels are used to adjust the distance between the camera assembly baseplate and the camera mount to obtain a good focus. The standard Pi Camera V2 can be focused from its factory setting (inifinity) to 2-3 cm by very carefully unscrewing the lens (with tweezers or the [3D printed tool](https://cad.onshape.com/documents/eaa285a111b058d718d92708/w/a8fac42fd42d5afdcea0de08/e/d43eff2b2bb073ce2917bf6d) . Then once mounted in the camera assembly the focuss wheels permit fine focus. If a hand lens is to be used the Pi Camera should be left focused to infinity.![Alt text](pictures/PICLampZeroV2.jpg)

#PiClampZero power and LED control circuit
The RPI-Cam-Web-Interface software manages the timeseries capture of images which are stored on the SD card. An [interface circuit]![Alt text](/pictures/Rpi_circuit.png) provides stable 5V power to the PiZeroW and allows the Pi to control the LED lights prior to taking a picture. The system runs from 12V (through barrel connector centre +ve). The LED strings run from 12V and a UBEC (ultra battery elliminator circuit) provides 5V for the PiZero (this a applied directly to the header pins and circumvents protection circuitry). **DO NOT APPLY POWER TO USB INPUT OF PIZERO while 12V supply connected!!**
 A 2N7000 MOSFET is used to control the power the the LEDs via pin 5 (GPOI3). A 10K pull down resistor holds the LEDs in the off state when not commanded 'on' by the Pi.

In hindsight the system could use 5V LEDs for top and bottom lights this would remove the need for the 12V to 5V DC/DC converter. A 5V USB power supply could be used to provide the power either through the barrel connector or using a mini-USB or USB-C connector mounted on the Top cap. The 5V would go to the input pin on the PiZero and to the 2N7000 mosfet which would switch the power to the top or bottom lights (5V rather than 12V).

 Two indicator LEDs are used to show the current state of the system.

+  The yellow one is illuminated whenever the PiZero is powered up (3.3v avaiable on the board).
+  The red one is illuminated when the Pi is active i.e. operating system not shut down. It is safest to shut down the Pi via the Web Interface and wait for the red LED to go off before removing power to reduce risk of corrupting the SD card. See setup details [^1]
 [interface circuit]![Alt text](/pictures/IMG_20190306_120012_5.jpg)

[^1]: In order for the red led to operate the Pi needs to have the UART enabled as this holds the TXD (pin 8) high while the operating system is alive. Edit boot/config and add enable_uart=1 at end.


#PiClampZero Raspberry Pi setup
The Pi zeros use 16Gb micro SD cards to hold the operating system and captured images. They are running on a full version of Raspbian Stretch. With a few packages removed (eg. Wolfram Alpha, Scratch) to reduce the img size to 4.5Gb.
The cameras are running [RPi-Cam-Web-Interface](https://elinux.org/RPi-Cam-Web-Interface) software to effectively turn them into web cams with a full web browser interface. This allows easy access to each camera via its IP address using a mobile phone or IPAD connected on the same WiFi network to assist in real time adjustment of camera focus and settings. The RPi-Cam-Web-Interface also manages the camera settings and image capture with a reasonably complex scheduling system.

The Raspberry Pi OS image and setup used in the PiClampZero can be found on cloudstor [PiClampZero OS image](https://cloudstor.aarnet.edu.au/plus/s/de23YAfMc9cQf4P). This can be burned onto a suitable SD card (I use 16Gb) using "Etcher" or some other software  designed for the job.


#Connecting to camera
Once booted up with this 'image' the Pi should boot up and expand the operating system to fill the SD card. This version causes the PiZero to establish a WiFi hotpoint with SSID "PiClampTest" you can connect to this with password 'DryLeafs'. Once connected point your browser to 192.168.4.1 which should bring the following screen ![Alt text](/pictures/PIcamscreen1.PNG)


You can then click on the HTML tab to bring up the camera interface. The main screen shows the live camera image and has buttons to take pictures, turn the LEDs on and off, start a timeseries etc. ![Alt text](/pictures/PIcamscreen2.PNG)

The PiZeroW do not have realtime clocks so it is necessary to set the clock via the RPi-Cam-Web-Interface on power up if you want the correct date/time stamp on image files. To do this select the 'System' tab then enter the date and time in the box can be in '2019-01-15 13:06' format and hit OK. The new date and time should appear on the camera image and the Pi should keep resonable time until reset or powered off.![Alt text](/pictures/PIcamscreen3.PNG).

The web interface allows you to control most of the settings for the camera (image size, white balance, exposure, etc) along with settings for timeseries. If you select 'Camera Setings' tab you should see the screen below. The default setting for timeseries is 5 mins (300secs) and most camera settings are set to automatic. Once a timeseries has been initiated (from the main screen) at the start of each 5 min period the PiClamp will turn on the LED's wait 5 seconds for the camera to adjust to the new light levels, take a picture  then turn off the LEDs   ![Alt text](/pictures/PIcamscreen4.PNG).

#Web interface LED control
The main illumination LEDs are switched on by the Pi either using the LED button in the web interface page or by the Pi itself 5 seconds prior to taking a picture. After taking a picture the LEDs are turned off. The RPi-Cam-Web-Interface allows for user defined macros and buttons to trigger macros. Two macros have been installed in /home/pi/RPi_Cam_Web_Interface/www/macros that turn [on](/misc/LED_ON.py) and [off](/misc/LED_OFF.py) the LED power. 


#Downloading large data files
Individual files can be downloaded from the camera to the device running the browser via WiFi using the web interface. Downloading large timeseries (collections of images) can run into problems using this method and it is probably better to connect a USB memory drive directly to the USB port (not the power port which has a simliar socket to the left) for data transfer. One way is to use an HDMI monitor plugged into the mini-HDMI port and a USB hub with keyboard, mouse and memory drive plugged into the USB port. You should then see the Pi's GUI interface and be able to transfer files from the internal SD card to the attached USB memory stick. By default the captured images are stored in '/var/www/html/media' ![Alt text](/pictures/PIcamscreen6.PNG)

#VNC connection
The PiClampZero also runs a VNC server so if you don't have an HDMI monitor it is still possible to get into the Pi to use the operating system to transfer files from the SD card to a USB device. You will need a VNC client such as 'VNC viewer' once connected to the PiClamp SSID you can use the VNC viewer to connect to 192.168.4.1 using password 'ClampCAM'. This should bring up the Pi GUI in a window on your laptop (or whatever).![Alt text](/pictures/PIcamscreen5.PNG)


#Issues?
When the RPi-Cam-Web-Interface is connected to a device and serving up a live camera feed the workload on the Pi is fairly high and will cause the Pi to heat up. It is probably advisable to limit the time connected and demanding a video stream to avoid overheating, especially where the PiClampZero is in a hot environment. 

This application tends to create a lot of pictures per run so it is necessary to ensure you remove old images to make space on the SD card for a new run. A possible alternative would be to alter the location that images are stored from the SD to a removeable USB drive. I'm not sure possible implications of doing this but [see here](https://elinux.org/RPi-Cam-Web-Interface#How_do_I_change_the_path_for_the_video_images_and_pictures.3F).


#Future directions
If there is an existing WiFi network then the PiClamps can be configured to connect to it and then they will be accessible to other devices on the network via whatever IP address they are allocated. Another possibility would be to use a Raspberry PI 3b to set up a Wifi network to manage the set of PiClamps. The Pi3b could be connected to another network via its ethernet port to provide a gateway to the PiClamps. In this situation the PiClamps can be given unique 'hostnames' when they are first initialised (SD card burnt and installed). This can be done through the Pi GUI using a HDMI monitor or VNC connection.![Alt_text](/pictures/PIcamscreen7.PNG)

#==========================================================================================================
#Below is relates to the PACE phenocam system but some could be used to monitor a group of PiClampZeros


#PICAM-master Raspberry Pi setup
A Raspberry Pi model 3B acts as the central manager for the PACECAM network. It is connected to the same WiFi network via its Wifi network interface and also has a connection to the university network via its ethernet network adaptor. The role of the PICAM-master is to collect images from all of the PACECAMS and upload them to a central database. It can also send commmands or files to all or selected PACECAMS in order to modify camera settings or image-capture shedules. 
The PICAM-master uses Ansible to manage the PACECAMs. Much of the initial set up and configuration was greatfully gleened from [this online source](https://github.com/calizarr/PhenoPiSight/blob/master/Installation_guide.md) based on a set up of over 100 Raspberry Pi cameras.
Ansible uses a hosts file to list and group all of the computers (PICAMS) that it controls. In our case the PACECAMS are grouped by heating and irrigation treatment in case we want to run a task on a treatment rather than all cameras, however most of the time tasks will be run on all PACECAMS.
Tasks are managed through Ansible playbooks which are YAML control files with a list of tasks to be carried out by each target host. The PICAM-master makes an ssh connection to a PACECAM and issues commands for it to run. 

#Ansible playbooks
The main playbook is [getImage.yml.](/Ansible_Playbooks/getImage.yml)

In which the sequence of tasks is:

+ rename image files to include hostname of local PACECAM  
+ remove unwanted thumbnail files from PACECAM image folder  
+ copy files from PACECAM to PICAM-master  
+ move files from PACECAM image folder to a backup folder on the PACECAM  

#PICAM-master shell scripts
there are a number of shell scripts that run via crontab on the PICAM-master to manage the data.
the main script is [getImage.sh](/Misc/shellscripts/getImage.sh)

+ This script opens a new log file (timestamped)
+ Pings all cameras from the shell (not from Ansible) to try to wake them up.
+ Runs ansible playbook getImages.yml
+ Counts how many images there are from each camera (where at least one image was downloaded).
+ Zips up images by shelter (so 4x8 =32 images per zip file) zipfiles.sh does this using todays date as file timestamp regardless of image timestamps. If lots of files fail to download from cameras on current date they will be zipped into later files when they do get downloaded.



#Misc notes
working notes are on google docs [here](https://docs.google.com/document/d/18WXofiWCKcc1WYL2ObTKU1IPdCUNz30FNO2YULknT-M/edit)


